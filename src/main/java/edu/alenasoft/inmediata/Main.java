package edu.alenasoft.inmediata;

import edu.alenasoft.inmediata.application.MallardDuck;
import edu.alenasoft.inmediata.application.RedheadDuck;
import edu.alenasoft.inmediata.application.RubberDuck;
import edu.alenasoft.inmediata.application.LoggerManager;
import edu.alenasoft.inmediata.client.Sprite;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;


class Main {
    private static final Logger logger = Logger.getLogger(LoggerManager.class.getName());

    public static void main(String[] args) {
        LoggerManager.init();
        logger.info("Hello coders!(Diseno inmediato)");
        MallardDuck mallard = new MallardDuck();
        RedheadDuck redhead = new RedheadDuck();
        RubberDuck rubber = new RubberDuck();
        List<Sprite> sprites = new ArrayList<>(0);
        sprites.add(mallard);
        sprites.add(redhead);
        sprites.add(rubber);

        sprites.forEach(Sprite::play);
    }
}
