package edu.alenasoft.inmediata.application;

import edu.alenasoft.inmediata.application.LoggerManager;
import java.util.logging.Logger;


public class RubberDuck extends Duck {
    private static final Logger logger = Logger.getLogger(LoggerManager.class.getName());

    @Override
    void display() {
        logger.info("RUBBER DUCK");
    }

    @Override
    public void quack() {
        logger.info("SQUEAK");
    }

}
