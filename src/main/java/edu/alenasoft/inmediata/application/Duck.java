package edu.alenasoft.inmediata.application;

import edu.alenasoft.inmediata.application.LoggerManager;
import edu.alenasoft.inmediata.client.Sprite;
import java.util.logging.Logger;


public abstract class Duck implements Sprite {
    private static final Logger logger = Logger.getLogger(LoggerManager.class.getName());

    public void quack() {
      logger.info("QUACK");
    }

    public void swim() {
      logger.info("SWIM");
    }

    public void fly() {
      logger.info("FLY");
    }

    @Override
    public void play() {
      this.display();
      this.swim();
      this.quack();
      this.fly();
    }


    abstract void display();

}
