package edu.alenasoft.inmediata.client;

@FunctionalInterface
public interface Sprite {

  void play();

}
