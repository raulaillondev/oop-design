package edu.alenasoft.interfaces.application;

@FunctionalInterface
public interface Flyable {

  void fly();

}
