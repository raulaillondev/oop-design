package edu.alenasoft.interfaces.application;

import edu.alenasoft.interfaces.application.Duck;
import edu.alenasoft.interfaces.application.Flyable;
import edu.alenasoft.interfaces.application.Quackable;
import edu.alenasoft.interfaces.application.LoggerManager;
import edu.alenasoft.interfaces.client.Sprite;
import java.util.logging.Logger;


public  class RedheadDuck extends Duck implements Sprite, Flyable, Quackable {
    private static final Logger logger = Logger.getLogger(LoggerManager.class.getName());

    @Override
    public void fly() {
      logger.info("FLY");
    }

    @Override
    public void quack() {
      logger.info("QUACK");
    }

    @Override
    void display() {
        logger.info("READHEAD DUCK");
    }

    @Override
    public void play() {
        super.play();
        this.quack();
        this.fly();
    }
}
