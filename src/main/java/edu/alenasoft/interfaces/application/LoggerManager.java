package edu.alenasoft.interfaces.application;

import java.util.logging.Logger;
import java.util.logging.Level;

public class LoggerManager {

    public static void init(){
        System.setProperty(
          "java.util.logging.SimpleFormatter.format",
          "%2$s %4$s: %5$s%n"
        );
        Logger l = Logger.getLogger("");
        l.setLevel(Level.CONFIG);
    }
 }
