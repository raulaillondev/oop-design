package edu.alenasoft.interfaces.application;

import edu.alenasoft.interfaces.application.LoggerManager;
import edu.alenasoft.interfaces.client.Sprite;
import java.util.logging.Logger;


public abstract class Duck implements Sprite {
    private static final Logger logger = Logger.getLogger(LoggerManager.class.getName());

    public void swim() {
      logger.info("SWIM");
    }

    @Override
    public void play() {
      this.display();
      this.swim();
    }

    abstract void display();

}
