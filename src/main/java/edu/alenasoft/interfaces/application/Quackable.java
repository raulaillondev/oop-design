package edu.alenasoft.interfaces.application;

@FunctionalInterface
public interface Quackable {

  void quack();

}
