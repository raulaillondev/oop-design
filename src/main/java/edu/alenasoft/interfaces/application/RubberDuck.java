package edu.alenasoft.interfaces.application;

import edu.alenasoft.interfaces.application.Duck;
import edu.alenasoft.interfaces.application.Quackable;
import edu.alenasoft.interfaces.application.LoggerManager;
import edu.alenasoft.interfaces.client.Sprite;
import java.util.logging.Logger;


public class RubberDuck extends Duck implements Sprite, Quackable {
    private static final Logger logger = Logger.getLogger(LoggerManager.class.getName());

    @Override
    void display() {
        logger.info("RUBBER DUCK");
    }

    @Override
    public void quack() {
        logger.info("SQUEAK");
    }

    @Override
    public void play() {
        super.play();
        this.quack();
    }

}
