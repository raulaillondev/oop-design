package edu.alenasoft.interfaces.application;

import edu.alenasoft.interfaces.application.Duck;
import edu.alenasoft.interfaces.application.LoggerManager;
import java.util.logging.Logger;

public class DecoyDuck extends Duck {
    private static final Logger logger = Logger.getLogger(LoggerManager.class.getName());

    @Override
    void display() {
        logger.info("DECOY DUCK");
    }

}
