package edu.alenasoft.interfaces;

import edu.alenasoft.interfaces.application.MallardDuck;
import edu.alenasoft.interfaces.application.RedheadDuck;
import edu.alenasoft.interfaces.application.RubberDuck;
import edu.alenasoft.interfaces.application.DecoyDuck;
import edu.alenasoft.interfaces.application.LoggerManager;
import edu.alenasoft.interfaces.client.Sprite;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;


class Main {
    private static final Logger logger = Logger.getLogger(LoggerManager.class.getName());

    public static void main(String[] args) {
        LoggerManager.init();
        logger.info("Hello coders!(Diseno interfaces)");
        MallardDuck mallard = new MallardDuck();
        RedheadDuck redhead = new RedheadDuck();
        RubberDuck rubber = new RubberDuck();
        DecoyDuck decoy = new DecoyDuck();
        List<Sprite> sprites = new ArrayList<>(0);
        sprites.add(mallard);
        sprites.add(redhead);
        sprites.add(rubber);
        sprites.add(decoy);

        sprites.forEach(Sprite::play);
    }
}
