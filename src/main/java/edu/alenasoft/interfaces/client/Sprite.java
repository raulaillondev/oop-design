package edu.alenasoft.interfaces.client;

@FunctionalInterface
public interface Sprite {

  void play();

}
