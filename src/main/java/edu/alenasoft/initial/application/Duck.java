package edu.alenasoft.initial.application;

import edu.alenasoft.initial.application.LoggerManager;
import edu.alenasoft.initial.client.Sprite;
import java.util.logging.Logger;


public abstract class Duck implements Sprite {
    private static final Logger logger = Logger.getLogger(LoggerManager.class.getName());

    public void quack() {
      logger.info("QUACK");
    }

    public void swim() {
      logger.info("SWIM");
    }

    @Override
    public void play() {
      this.display();
      this.swim();
      this.quack();
    }

    abstract void display();

}
