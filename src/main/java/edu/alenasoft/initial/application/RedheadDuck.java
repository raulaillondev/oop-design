package edu.alenasoft.initial.application;

import edu.alenasoft.initial.application.LoggerManager;
import java.util.logging.Logger;


public  class RedheadDuck extends Duck {
    private static final Logger logger = Logger.getLogger(LoggerManager.class.getName());

    @Override
    void display() {
        logger.info("READHEAD DUCK");
    }

}
