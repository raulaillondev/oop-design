package edu.alenasoft.initial.client;

@FunctionalInterface
public interface Sprite {

  void play();

}
