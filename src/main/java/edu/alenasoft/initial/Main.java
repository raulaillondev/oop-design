package edu.alenasoft.initial;

import edu.alenasoft.initial.application.MallardDuck;
import edu.alenasoft.initial.application.RedheadDuck;
import edu.alenasoft.initial.application.LoggerManager;
import edu.alenasoft.initial.client.Sprite;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;


class Main {
    private static final Logger logger = Logger.getLogger(LoggerManager.class.getName());

    public static void main(String[] args) {
        LoggerManager.init();
        logger.info("Hello coders!(Diseño Inicial)");
        MallardDuck mallard = new MallardDuck();
        RedheadDuck redhead = new RedheadDuck();
        List<Sprite> sprites = new ArrayList<>(0);
        sprites.add(mallard);
        sprites.add(redhead);

        sprites.forEach(Sprite::play);
    }
}
